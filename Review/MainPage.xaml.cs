﻿using MetroLog;
using Newtonsoft.Json;
using Review.Classes;
using ReviewAdmin.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Review
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private ILogger Log = LogManagerFactory.DefaultLogManager.GetLogger<MainPage>();

        public DispatcherTimer CheckQuestionsTimer { get; set; }

        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            this.Log.Info("App started");

            var credential = CredentialManager.Instance.GetCredentials();

            if (credential == null)
            {
                await new ContentDialog1().ShowAsync();
            }

            var hbResponse = await HeartBeat.Instance.SendAsync();
            while (hbResponse == HttpStatusCode.Unauthorized)
            {
                await new ContentDialog1().ShowAsync();
                hbResponse = await HeartBeat.Instance.SendAsync();
            }
;

            HeartBeat.Instance.Start();

            CheckQuestionsTimer = new DispatcherTimer();
            CheckQuestionsTimer.Tick += CheckQuestionsTimer_TickAsync;
            CheckQuestionsTimer.Interval = new TimeSpan(0, 0, 1);
            CheckQuestionsTimer.Start();
        }

        private async void CheckQuestionsTimer_TickAsync(object sender, object e)
        {
            this.Log.Info("QuestionTimerTick");

            if (CheckQuestionsTimer.Interval == new TimeSpan(0, 0, 1))
            {
                CheckQuestionsTimer.Interval = new TimeSpan(0, 0, Config.Instance.Settings.GetQuestionsAtMainPageInterval);
                this.Log.Info("CheckQuestions Timer interval changed to " + Config.Instance.Settings.GetQuestionsAtMainPageInterval);
            }

            if (await QuestionsCollector.Instance.CollectQuestions())
            {
                this.Log.Info("Questions collected, navigate to BannersPage");
                CheckQuestionsTimer.Stop();
                this.Frame.Navigate(typeof(BannersPage));
            }
        }
    }
}
