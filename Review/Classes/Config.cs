﻿using MetroLog;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking.Connectivity;
using Windows.Storage;

namespace Review.Classes
{
    class Config
    {
        private static Config instance = null;
        private static readonly object padlock = new object();

        private ILogger Log = LogManagerFactory.DefaultLogManager.GetLogger<MainPage>();

        //private Settings settings;

        public static Config Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Config();
                    }
                    return instance;
                }
            }
        }

        public Settings Settings { get; set; }

        public async Task ReadSettingsFromFileAsync()
        {
            try
            {
                StorageFolder folder = ApplicationData.Current.LocalFolder;
                StorageFile file = await folder.GetFileAsync("settings.json");
                string Json = await FileIO.ReadTextAsync(file);
                Settings = JsonConvert.DeserializeObject<Settings>(Json);
            }
            catch (Exception ex)
            {
                this.Log.Error("Cannot read config file!", ex);
            }
        }

        public async Task WriteDefaultSettingsToFileAsync()
        {
            Config.Instance.Settings = new Settings()
            {
                TerminalName = GetHostName(),
                TerminalDescription = "",
                //AnswersUrl = "http://localhost:59643/Api/Answers",
                //QuestionsUrl = "http://localhost:59643/Api/Questions",
                //HeartBeatUrl = "http://localhost:59643/Api/Terminals",
                AnswersUrl = "http://review.termal.local:5000/Api/Answers",
                QuestionsUrl = "http://review.termal.local:5000/Api/Questions",
                HeartBeatUrl = "http://review.termal.local:5000/Api/Terminals",
                HttpTimeout = 10,
                GetQuestionsAtMainPageInterval = 30,
                GetQuestionsAtBannersPageInterval = 3600,
                AnswersRetryInterval = 3600,
                TimeToAnswer = 30,
                HeartBeatInterval = 60
            };
            try
            {
                StorageFolder folder = ApplicationData.Current.LocalFolder;
                StorageFile file = await folder.CreateFileAsync("settings.json", CreationCollisionOption.ReplaceExisting);

                using (JsonTextWriter jsonwriter = new JsonTextWriter(new StreamWriter(await file.OpenStreamForWriteAsync())))
                {
                    var serializer = new JsonSerializer();
                    serializer.TypeNameHandling = TypeNameHandling.None;
                    serializer.Formatting = Formatting.Indented;
                    serializer.Serialize(jsonwriter, Config.Instance.Settings);
                }
            }
            catch (Exception ex)
            {
                this.Log.Error("Cannot write default config file!", ex);
            }
        }

        private string GetHostName()
        {
            var hostNames = NetworkInformation.GetHostNames();
            var localName = hostNames.FirstOrDefault(name => name.DisplayName.Contains(".local"));
            return localName.DisplayName.Replace(".local", "");
        }

        internal async Task<bool> SettingsFileExistsAsync()
        {
            StorageFolder folder = ApplicationData.Current.LocalFolder;
            var file = await folder.TryGetItemAsync("settings.json");
            return file != null;
        }
    }
}
