﻿using MetroLog;
using Newtonsoft.Json;
using ReviewAdmin.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Search;

namespace Review.Classes
{
    class AnswersSender
    {
        private static AnswersSender instance = null;
        private static readonly object padlock = new object();

        private StorageFolder localFolder;

        private Timer timer;

        private MetroLog.ILogger Log = LogManagerFactory.DefaultLogManager.GetLogger<MainPage>();

        private AnswersSender()
        {
            localFolder = ApplicationData.Current.LocalFolder;
        }

        public static AnswersSender Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new AnswersSender();
                    }
                    return instance;
                }
            }
        }

        public void StartTimer()
        {
            if (timer == null)
            {
                timer = new Timer(
                    Timer_Tick,
                    null,
                    Config.Instance.Settings.HttpTimeout * 1000,
                    Config.Instance.Settings.AnswersRetryInterval * 1000
                    ); //  (callback, , callback-param, 1.run, other runs)
            }
        }

        private async void Timer_Tick(object state)
        {
            var answersFolder = await localFolder.CreateFolderAsync("Answers", CreationCollisionOption.OpenIfExists);

            var queryOptions = new QueryOptions(CommonFileQuery.DefaultQuery, new[] { ".json" });
            var query = answersFolder.CreateFileQueryWithOptions(queryOptions);
            var files = await query.GetFilesAsync();
            this.Log.Info(String.Format("Sending answer files ({0} files)", files.Count()));
            foreach (var file in files)
            {
                await SendFileAsync(file);
            }
        }


        private async Task WriteFileAsync(Answer answer)
        {
            var filename = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + ".json";
            var answersFolder = await localFolder.CreateFolderAsync("Answers", CreationCollisionOption.OpenIfExists);
            StorageFile file = await answersFolder.CreateFileAsync(filename, CreationCollisionOption.OpenIfExists);
            using (JsonTextWriter jsonwriter = new JsonTextWriter(new StreamWriter(await file.OpenStreamForWriteAsync())))
            {
                var serializer = new JsonSerializer();
                serializer.TypeNameHandling = TypeNameHandling.None;
                serializer.Formatting = Formatting.Indented;
                serializer.Serialize(jsonwriter, answer);
            }
        }

        private async Task<string> ReadFileAsync(StorageFile file)
        {
            using (var stream = await file.OpenStreamForReadAsync())
            {
                using (var reader = new StreamReader(stream))
                {
                    string json = reader.ReadToEnd();
                    return json;
                }
            }
        }

        private async Task SendFileAsync(StorageFile file)
        {
            try
            {
                using (var httpClient = new ApiClient())
                {
                    var json = await ReadFileAsync(file);
                    var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
                    var httpResponse = await httpClient.PostAsync(Config.Instance.Settings.AnswersUrl, httpContent);
                    this.Log.Info("Answers API response statuscode: " + httpResponse.StatusCode);

                    if (httpResponse.StatusCode == System.Net.HttpStatusCode.Created)
                    {
                        await file.DeleteAsync();
                        this.Log.Info("Answer file deleted");
                    }
                    else
                    {
                        this.Log.Error("Answer file NOT deleted! Error: "
                            + await httpResponse.Content.ReadAsStringAsync());
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.Error("AnswerSend has thrown exception: ", ex);
            }
        }

        internal async Task SendAsync(Answer answer)
        {
            try
            {
                using (var httpClient = new ApiClient())
                {
                    var json = JsonConvert.SerializeObject(answer);
                    var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
                    var httpResponse = await httpClient.PostAsync(Config.Instance.Settings.AnswersUrl, httpContent);
                    this.Log.Info("Answers API response (first attempt) statuscode: " + httpResponse.StatusCode);
                    if (httpResponse.StatusCode == System.Net.HttpStatusCode.Created)
                    {
                        this.Log.Info("Answer sent (FirstSend)");
                    }
                    else
                    {

                        this.Log.Error("Answers API response is not 201, creating file. Error: "
                            + await httpResponse.Content.ReadAsStringAsync());
                        await WriteFileAsync(answer);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.Error("AnswerSend has thrown exception, creating file: ", ex);
                await WriteFileAsync(answer);
            }
        }
    }
}
