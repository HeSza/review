﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Review.Classes
{
    class CredentialManager
    {
        private static CredentialManager instance = null;
        private static readonly object padlock = new object();

        private CredentialManager()
        {
        }

        public static CredentialManager Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new CredentialManager();
                    }
                    return instance;
                }
            }
        }


        public void AddCredentialToLocker(string user, string password)
        {
            var vault = new Windows.Security.Credentials.PasswordVault();
            RemoveAllCredentials(vault);
            vault.Add(new Windows.Security.Credentials.PasswordCredential("Review", user, password));
        }

        private void RemoveAllCredentials(Windows.Security.Credentials.PasswordVault vault)
        {
            try // FindAllByResource throws Exception when empty
            {
                var credentialList = vault.FindAllByResource("Review");
                foreach (var cred in credentialList)
                {
                    vault.Remove(cred);
                }
            }
            catch (Exception)
            {
            }

        }

        public Windows.Security.Credentials.PasswordCredential GetCredentials()
        {
            Windows.Security.Credentials.PasswordCredential credential = null;

            try
            {
                var vault = new Windows.Security.Credentials.PasswordVault();
                var credentialList = vault.FindAllByResource("Review");
                if (credentialList.Count == 1)
                {
                    credential = credentialList[0];
                    credential.RetrievePassword();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return credential;
        }

    }
}
