﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Review.Classes
{
    class Settings
    {
        public string TerminalName { get; set; }
        public string TerminalDescription { get; set; }
        public string AnswersUrl { get; set; }
        public string QuestionsUrl { get; set; }
        public string HeartBeatUrl { get; set; }
        public int HttpTimeout { get; set; }
        public int GetQuestionsAtMainPageInterval { get; set; }
        public int GetQuestionsAtBannersPageInterval { get; set; }
        public int AnswersRetryInterval { get; set; }
        public int TimeToAnswer { get; set; }
        public int HeartBeatInterval { get; set; }
    }
}
