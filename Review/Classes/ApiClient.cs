﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Review.Classes
{
    class ApiClient: HttpClient
    {
        public ApiClient() : base ()
        {
            var cred = CredentialManager.Instance.GetCredentials();

            this.Timeout = new TimeSpan(0, 0, Config.Instance.Settings.HttpTimeout);
            var byteArray = Encoding.ASCII.GetBytes(String.Format("{0}:{1}", cred.UserName, cred.Password));
            this.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

        }
    }
}
