﻿using MetroLog;
using MetroLog.WinRT;
using Newtonsoft.Json;
using ReviewAdmin.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Xaml;

namespace Review.Classes
{
    class QuestionsCollector
    {
        private static QuestionsCollector instance = null;
        private static readonly object padlock = new object();

        private StorageFolder localFolder;

        private MetroLog.ILogger Log = LogManagerFactory.DefaultLogManager.GetLogger<MainPage>();

        private QuestionsCollector()
        {
            localFolder = ApplicationData.Current.LocalFolder;
        }

        public static QuestionsCollector Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new QuestionsCollector();
                    }
                    return instance;
                }
            }
        }

        public List<Question> Questions { get; internal set; }

        public async Task<bool> CollectQuestions()
        {
            var questionsFromServer = await GetQuestionsAsync();

            if (questionsFromServer != null)
            {
                Questions = questionsFromServer;
                await WriteFileAsync(questionsFromServer);
                this.Log.Info("Questions got from server");
                return true;
            } else
            {
                this.Log.Warn("Getting questions from server failed");
            }

            var questionsFromFile = await ReadQuestionsAsync();

            if (questionsFromFile != null)
            {
                Questions = questionsFromFile;
                this.Log.Info("Questions read from file");
                return true;
            }
            this.Log.Warn("Reading questions from file failed!");
            return false;
        }

        private async Task<List<Question>> GetQuestionsAsync()
        {
            using (var client = new ApiClient())
            {
                string content = "";
                try
                {
                    var httpResponse = await client.GetAsync(Config.Instance.Settings.QuestionsUrl);
                    content = await httpResponse.Content.ReadAsStringAsync();
                    this.Log.Info("Questions API response statuscode: " + httpResponse.StatusCode);
                    return JsonConvert.DeserializeObject<List<Question>>(content);
                }
                catch (Exception ex)
                {
                    this.Log.Error("GetQuestionsAsync has thrown exception: ", ex);
                    this.Log.Error("Response body: " + content);
                    return null;
                }
            }
        }

        private async Task<List<Question>> ReadQuestionsAsync()
        {
            List<Question> questions;
            try
            {
                StorageFolder folder = ApplicationData.Current.LocalFolder;
                StorageFile file = await folder.GetFileAsync(@"questions.json");
                string Json = await FileIO.ReadTextAsync(file);
                questions = JsonConvert.DeserializeObject<List<Question>>(Json);
            }
            catch
            {
                return null;
            }
            return questions;
        }

        private async Task WriteFileAsync(List<Question> questions)
        {
            StorageFile file = await localFolder.CreateFileAsync(@"questions.json", CreationCollisionOption.ReplaceExisting);
            using (JsonTextWriter jsonwriter = new JsonTextWriter(new StreamWriter(await file.OpenStreamForWriteAsync())))
            {
                var serializer = new JsonSerializer();
                serializer.TypeNameHandling = TypeNameHandling.None;
                serializer.Formatting = Formatting.Indented;
                serializer.Serialize(jsonwriter, questions);
            }
        }
    }
}
