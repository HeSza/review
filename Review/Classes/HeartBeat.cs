﻿using MetroLog;
using Newtonsoft.Json;
using ReviewAdmin.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace Review.Classes
{
    class HeartBeat
    {
        private static HeartBeat instance = null;
        private static readonly object padlock = new object();

        private DispatcherTimer Timer = new DispatcherTimer();

        private MetroLog.ILogger Log = LogManagerFactory.DefaultLogManager.GetLogger<MainPage>();

        private readonly Terminal Terminal = new Terminal()
        {
            Name = Config.Instance.Settings.TerminalName,
            Description = Config.Instance.Settings.TerminalName
        };

        public static HeartBeat Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new HeartBeat();
                    }
                    return instance;
                }
            }
        }

        public void Start()
        {
            Timer.Interval = new TimeSpan(0, 0, Config.Instance.Settings.HeartBeatInterval);
            Timer.Tick += Timer_TickAsync;
            Timer.Start();
        }

        public async Task<HttpStatusCode?> SendAsync()
        {
            try
            {
                using (var apiClient = new ApiClient())
                {
                    var httpContent = new StringContent(JsonConvert.SerializeObject(Terminal), Encoding.UTF8, "application/json");
                    HttpResponseMessage httpResponse = await apiClient.PostAsync(Config.Instance.Settings.HeartBeatUrl, httpContent);
                    this.Log.Info("HeartBeat API response statuscode: " + httpResponse.StatusCode);
                    if (httpResponse.StatusCode != HttpStatusCode.Created)
                    {
                        this.Log.Error("HeartBeat API error: " + await httpResponse.Content.ReadAsStringAsync());
                    }
                    return httpResponse.StatusCode;
                }
            }
            catch (Exception ex)
            {
                this.Log.Warn("HeartBeat has thrown exception: ", ex);
                return null;
            }
        }

        public void Stop()
        {
            Timer.Stop();
        }

        private async void Timer_TickAsync(object sender, object e)
        {
            await SendAsync();
        }
    }
}
