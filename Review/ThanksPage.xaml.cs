﻿using MetroLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Review
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ThanksPage : Page
    {

        private DispatcherTimer Timer;

        private ILogger Log = LogManagerFactory.DefaultLogManager.GetLogger<BannersPage>();

        public ThanksPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.Log.Info("Page loaded");

            if ((Lang) e.Parameter == Lang.Hungarian)
            {
                HunText.Visibility = Visibility.Visible;
                EngText.Visibility = Visibility.Collapsed;
            }
            else
            {
                HunText.Visibility = Visibility.Collapsed;
                EngText.Visibility = Visibility.Visible;
            }

            Timer = new DispatcherTimer()
            {
                Interval = new TimeSpan(0, 0, 5)
            };

            Timer.Tick += Timer_Tick;
            Timer.Start();

            base.OnNavigatedTo(e);
        }

        private void Timer_Tick(object sender, object e)
        {
            Timer.Stop();
            this.Frame.Navigate(typeof(BannersPage));
        }
    }
}
