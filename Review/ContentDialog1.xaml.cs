﻿using Review.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Content Dialog item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Review
{
    public sealed partial class ContentDialog1 : ContentDialog
    {

        public static readonly DependencyProperty UserNameProperty = DependencyProperty.Register(
           "UserName", typeof(string), typeof(ContentDialog1), new PropertyMetadata(default(string)));

        public static readonly DependencyProperty PasswordProperty = DependencyProperty.Register(
           "Password", typeof(string), typeof(ContentDialog1), new PropertyMetadata(default(string)));

        public ContentDialog1()
        {
            this.InitializeComponent();
        }

        public string UserName
        {
            get { return (string)GetValue(UserNameProperty); }
            set { SetValue(UserNameProperty, value); }
        }

        public string Password
        {
            get { return (string)GetValue(PasswordProperty); }
            set { SetValue(PasswordProperty, value); }
        }

        private void ContentDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            CredentialManager.Instance.AddCredentialToLocker(UserName, Password);
        }

        private void ContentDialog_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
        }

        private void TextBlock_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                CredentialManager.Instance.AddCredentialToLocker(UserName, Password);
            }
        }
    }
}
