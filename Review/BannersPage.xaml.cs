﻿using MetroLog;
using Review.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Review
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class BannersPage : Page
    {
        public DispatcherTimer Timer { get; set; }

        private ILogger Log = LogManagerFactory.DefaultLogManager.GetLogger<BannersPage>();

        public BannersPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.Log.Info("Page loaded");
            Timer = new DispatcherTimer();
            Timer.Tick += PollTimer_TickAsync;
            Timer.Interval = new TimeSpan(0, 0, Config.Instance.Settings.GetQuestionsAtBannersPageInterval);
            Timer.Start();
        }

        private async void PollTimer_TickAsync(object sender, object e)
        {
            this.Log.Info("Collecting questions");
            await QuestionsCollector.Instance.CollectQuestions();
        }

        private void HunButton_Click(object sender, RoutedEventArgs e)
        {
            Timer.Stop();
            this.Frame.Navigate(typeof(QuestionPage), Lang.Hungarian);
            this.Log.Info("Hungarian banner selected");
        }

        private void EngButton_Click(object sender, RoutedEventArgs e)
        {
            Timer.Stop();
            this.Frame.Navigate(typeof(QuestionPage), Lang.English);
            this.Log.Info("English banner selected");
        }
    }
}
