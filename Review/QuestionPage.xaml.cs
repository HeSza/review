﻿using MetroLog;
using Newtonsoft.Json;
using Review.Classes;
using ReviewAdmin.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Review
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class QuestionPage : Page
    {

        private DispatcherTimer TimeToAnswerTimer { get; set; }

        private Question ActQuestion { get; set; }

        private Lang _actLang;

        private ILogger Log = LogManagerFactory.DefaultLogManager.GetLogger<BannersPage>();

        public QuestionPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.Log.Info("Page loaded");
            _actLang = (Lang)e.Parameter;
            DisplayQuestion();

            TimeToAnswerTimer = new DispatcherTimer();
            TimeToAnswerTimer.Tick += AnswerTimer_Tick;
            TimeToAnswerTimer.Interval = new TimeSpan(0, 0, Config.Instance.Settings.TimeToAnswer);
            TimeToAnswerTimer.Start();

            base.OnNavigatedTo(e);
        }

        private void AnswerTimer_Tick(object sender, object e)
        {
            TimeToAnswerTimer.Stop();
            this.Frame.Navigate(typeof(BannersPage));
            this.Log.Info("Time up, back to BannersPage");
        }

        private void DisplayQuestion()
        {
            if (_actLang == Lang.Hungarian)
            {
                TextHun.Visibility = Visibility.Visible;
                TextEng.Visibility = Visibility.Collapsed;
                RateNoTextBlock.Text = "Nem";
                RateYesTextBlock.Text = "Igen";
            }
            else
            {
                TextHun.Visibility = Visibility.Collapsed;
                TextEng.Visibility = Visibility.Visible;
                RateNoTextBlock.Text = "No";
                RateYesTextBlock.Text = "Yes";
            }

            Random random = new Random();
            int r = random.Next(QuestionsCollector.Instance.Questions.Count());
            ActQuestion = QuestionsCollector.Instance.Questions[r];
            if (ActQuestion.Type == QuestionType.FiveAnswers)
            {
                StackPanelRate5.Visibility = Visibility.Visible;
                StackPanelYesNo.Visibility = Visibility.Collapsed;
            }
            else
            {
                StackPanelRate5.Visibility = Visibility.Collapsed;
                StackPanelYesNo.Visibility = Visibility.Visible;
            }

            this.Log.Info("Question displayed");
        }

        private void TextBlock_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            double desiredHeight = QuestionRow.ActualHeight;

            if ((sender as TextBlock).ActualHeight > desiredHeight)
            {
                double fontsizeMultiplier = Math.Sqrt(desiredHeight / (sender as TextBlock).ActualHeight);
                (sender as TextBlock).FontSize = Math.Floor((sender as TextBlock).FontSize * fontsizeMultiplier);
            }
            (sender as TextBlock).Height = desiredHeight;
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Log.Info("Back button selected");
            TimeToAnswerTimer.Stop();
            this.Frame.Navigate(typeof(BannersPage));
        }

        private async void RateButton_Click(object sender, RoutedEventArgs e)
        {
            this.Log.Info(String.Format("Rate button with tag:{0} selected", (string)(sender as Button).Tag));
            TimeToAnswerTimer.Stop();

            Answer answer = new Answer()
            {
                QuestionId = ActQuestion.Id,
                Lang = _actLang == Lang.Hungarian ? "magyar" : "angol",
                TerminalName = Config.Instance.Settings.TerminalName,
                Value = (string)(sender as Button).Tag
            };


            AnswersSender.Instance.SendAsync(answer);

            this.Frame.Navigate(typeof(ThanksPage), _actLang);
        }
    }
}
