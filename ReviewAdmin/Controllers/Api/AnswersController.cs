﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ReviewAdmin.Data;
using ReviewAdmin.Models;

namespace ReviewAdmin.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnswersController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public AnswersController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Answers
        [HttpGet]
        public IEnumerable<AnswerListVm> GetAnswer()
        {
            var answersWithQuestions = _context.Answer.Include(a => a.Question);
            var li = new List<AnswerListVm>();

            foreach (var a in answersWithQuestions)
            {
                var av = new AnswerListVm()
                {
                    Id = a.Id,
                    QuestionName = a.Question.Name,
                    AnswerValue = a.Value,
                    TerminalName = a.TerminalName,
                    Lang = a.Lang,
                    DateTime = a.DateTime.ToString("yyyy.MM.dd HH:mm")
                    // config file és log jön
                };
                li.Add(av);
            }
            return li;
        }

        // POST: api/Answers
        [HttpPost]
        public async Task<IActionResult> PostAnswer([FromBody] Answer answer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if ( _context.Terminal.FirstOrDefault(t => t.Name == answer.TerminalName) == null)
            {
                Terminal terminal = new Terminal
                {
                    Name = answer.TerminalName,
                    LastAlive = DateTime.Now,
                    Answers = new List<Answer>
                    {
                        answer
                    }
                };
                _context.Add(terminal);
            }

            answer.DateTime = DateTime.Now;
            _context.Answer.Add(answer);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAnswer", new { id = answer.Id }, answer);
        }
    }
}