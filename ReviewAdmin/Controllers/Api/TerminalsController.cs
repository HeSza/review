﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ReviewAdmin.Data;
using ReviewAdmin.Models;

namespace ReviewAdmin.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class TerminalsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public TerminalsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // POST: api/Terminals
        [HttpPost]
        public async Task<IActionResult> PostTerminal([FromBody] Terminal terminal)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var terminalToModify = _context.Terminal.FirstOrDefault(t => t.Name == terminal.Name);
            if (terminalToModify == null)
            {
                terminal.LastAlive = DateTime.Now;
                _context.Terminal.Add(terminal);
            }
            else
            {
                terminalToModify.LastAlive = DateTime.Now;
            }

            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTerminal", new { id = terminal.Name }, terminal);
        }
    }
}