﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReviewAdmin.Controllers.Api
{
    public class AnswerListVm
    {
        public int Id { get; set; }
        public string QuestionName { get; set; }

        string _answerValue;
        public string AnswerValue
        {
            get { return _answerValue; }
            set
            {
                if (value == "0")
                {
                    value = "Nem";
                }
                else if (value == "10")
                {
                    value = "Igen";
                }
                _answerValue = value;
            }
        }

        public string Lang { get; set; }
        public string TerminalName { get; set; }
        public string DateTime { get; set; }
    }
}
