﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ReviewAdmin.Models
{
    public enum QuestionType
    {
        YesOrNo = 1,
        FiveAnswers = 2
    }

    public class Question
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Add meg a kérdés nevét"), Display(Name = "Cím")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Add meg a kérdés szövegét"), Display(Name = "Kérdés (hu)")]
        public string TextHun { get; set; }

        [Required(ErrorMessage = "Add meg a kérdés szövegét"), Display(Name = "Kérdés (en)")]
        public string TextEng { get; set; }

        [Required(ErrorMessage = "Add meg a kérdés típusát"), Display(Name = "Típus")]
        public QuestionType Type { get; set; }

        [Required(ErrorMessage = "Add meg, hogy a kérdés aktív-e"), Display(Name = "Aktív")]
        public bool IsActive { get; set; }

        public ICollection<Answer> Answers { get; set; }
    }
}
