﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ReviewAdmin.Models
{
    public class Answer
    {
        public int Id { get; set; }

        [Required]
        public int QuestionId { get; set; }

        [Display(Name = "Kérdés")]
        public Question Question { get; set; }

        [Display(Name = "Válasz")]
        public string Value { get; set; }

        [Display(Name = "Nyelv")]
        public string Lang { get; set; }

        [Required]
        public string TerminalName { get; set; }

        [Display(Name = "Terminál")]
        public Terminal Terminal { get; set; }

        [Display(Name = "Időpont")]
        public DateTime DateTime { get; set; }

    }
}
