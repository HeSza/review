﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ReviewAdmin.Models
{
    public class Terminal
    {
        [Key, Required, MaxLength(15), Display(Name = "Név"),]
        public string Name { get; set; }

        [Display(Name = "Leírás"), MaxLength(50)]
        public string Description { get; set; }

        [Display(Name = "Bejelentkezés"),  DataType(DataType.DateTime)]
        public DateTime LastAlive { get; set; }

        [Display(Name = "Válaszok")]
        public ICollection<Answer> Answers { get; set; }
    }
}
