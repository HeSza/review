﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReviewAdmin.Classes
{
    public class AuthenticationMiddleware
    {
        private readonly RequestDelegate _next;
        string _user;
        string _password;

        public AuthenticationMiddleware(RequestDelegate next, IOptions<MiddlewareCredential> options)
        {
            _next = next;
            _user = options.Value.User;
            _password = options.Value.Password;
        }

        public async Task Invoke(HttpContext context)
        {
            // https://www.devtrends.co.uk/blog/conditional-middleware-based-on-request-in-asp.net-core
            //if (context.Request.Path.Value.StartsWith("/Api/"))
            if (context.Request.Path.StartsWithSegments("/Api"))
            {
                string authHeader = context.Request.Headers["Authorization"];
                if (authHeader != null && authHeader.StartsWith("Basic"))
                {
                    //Extract credentials
                    string encodedUsernamePassword = authHeader.Substring("Basic ".Length).Trim();
                    Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                    string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));

                    int seperatorIndex = usernamePassword.IndexOf(':');

                    var username = usernamePassword.Substring(0, seperatorIndex);
                    var password = usernamePassword.Substring(seperatorIndex + 1);

                    if (username == _user && password == _password)
                    {
                        await _next.Invoke(context);
                    }
                    else
                    {
                        context.Response.StatusCode = 401; //Unauthorized
                        return;
                    }
                }
                else
                {
                    // no authorization header
                    context.Response.StatusCode = 401; //Unauthorized
                    return;
                }
            }
            else
            {
                await _next.Invoke(context);
            }
        }
    }
}
