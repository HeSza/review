﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ReviewAdmin.Models;

namespace ReviewAdmin.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Answer>()
                .HasOne(a => a.Question)
                .WithMany(q => q.Answers)
                .HasForeignKey(a => a.QuestionId);

            builder.Entity<Answer>()
                .HasOne(a => a.Terminal)
                .WithMany(t => t.Answers)
                .HasForeignKey(a => a.TerminalName);


            builder.Entity<Answer>().
                Property(a => a.DateTime)
                .HasColumnType("datetime");
        }

        public DbSet<ReviewAdmin.Models.Terminal> Terminal { get; set; }
        public DbSet<ReviewAdmin.Models.Question> Question { get; set; }
        public DbSet<ReviewAdmin.Models.Answer> Answer { get; set; }
    }
}
