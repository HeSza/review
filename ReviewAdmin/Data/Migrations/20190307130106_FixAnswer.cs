﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ReviewAdmin.Data.Migrations
{
    public partial class FixAnswer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TerminalId",
                table: "Answer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "TerminalName",
                table: "Answer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Answer_TerminalName",
                table: "Answer",
                column: "TerminalName");

            migrationBuilder.AddForeignKey(
                name: "FK_Answer_Terminal_TerminalName",
                table: "Answer",
                column: "TerminalName",
                principalTable: "Terminal",
                principalColumn: "Name",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Answer_Terminal_TerminalName",
                table: "Answer");

            migrationBuilder.DropIndex(
                name: "IX_Answer_TerminalName",
                table: "Answer");

            migrationBuilder.DropColumn(
                name: "TerminalId",
                table: "Answer");

            migrationBuilder.DropColumn(
                name: "TerminalName",
                table: "Answer");
        }
    }
}
