﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ReviewAdmin.Data.Migrations
{
    public partial class test : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Answer_Terminal_TerminalName",
                table: "Answer");

            migrationBuilder.DropColumn(
                name: "TerminalId",
                table: "Answer");

            migrationBuilder.AlterColumn<string>(
                name: "TerminalName",
                table: "Answer",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Answer_Terminal_TerminalName",
                table: "Answer",
                column: "TerminalName",
                principalTable: "Terminal",
                principalColumn: "Name",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Answer_Terminal_TerminalName",
                table: "Answer");

            migrationBuilder.AlterColumn<string>(
                name: "TerminalName",
                table: "Answer",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<int>(
                name: "TerminalId",
                table: "Answer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_Answer_Terminal_TerminalName",
                table: "Answer",
                column: "TerminalName",
                principalTable: "Terminal",
                principalColumn: "Name",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
