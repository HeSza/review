## Review - Kiosk terminálokon futó vendég-értékelésre szolgáló szoftver.

---

A kliens szoftver egy UWP app, kifejezetten Windows publikus kiosk módban tüörténő használatra.
API hívásokkal kommunikál, megjeleníti a szervertől lekért kérdéseket, és elküldi (vagy cache-eli) a válaszokat.

A kiszolgáló szoftver egy .NET Core 2.1 alapú web-application, ennek használatával lehet szerkeszteni a terminálokat, kérdéseket, és válaszokat.
Tartalmazza az API végpontok implemetációját is.

---

## Függőségek, külső library-k

Kliens:

1. .NETCore UWP
2. Newtonsoft.JSON 12.0.1
3. Metrolog 1.0.1
4. jQuery 3.3.1
5. Bootstrap 4.1.1
6. DataTables 1.10.18 + Buttons 1.5.4 + HTML5 export 1.5.4


Szerver :

1. .NETCore.App 2.1